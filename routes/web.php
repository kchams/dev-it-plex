<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('home');;

//Auth::routes();

Route::prefix('user')->group(function () {
    Route::get('login', 'User\Auth\LoginController@showLoginForm')->name('user.login');
    Route::post('login', 'User\Auth\LoginController@login');
    Route::post('logout', 'User\Auth\LoginController@logout')->name('user.logout');

    Route::get('register', 'User\Auth\RegisterController@showRegistrationForm')->name('user.register');
    Route::post('register', 'User\Auth\RegisterController@register');
    Route::get('home', 'User\HomeController@index')->name('user.home');

    Route::get('passForgot', 'User\Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.request');
    Route::get('passReset', 'User\Auth\ResetPasswordController@showResetForm')->name('user.password.reset');
    Route::get('emailVerify', 'User\Auth\VerificationController@show')->name('user.verification.resend');

    Route::resource('products','User\Auth\ProductController')->middleware('auth:user');
});

Route::prefix('admin')->group(function () {
    Route::get('login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\Auth\LoginController@login');
    Route::post('logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    Route::get('register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'Admin\Auth\RegisterController@register');
    Route::get('home', 'Admin\HomeController@index')->name('admin.home');

    Route::get('passForgot', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::get('passReset', 'Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::get('emailVerify', 'Admin\Auth\VerificationController@show')->name('admin.verification.resend');

    Route::resource('categories','Admin\Auth\CategoryController')->middleware('auth:admin');
});

/*
Route::prefix('categories')->group(function () {
    Route::get('/home', 'Admin\Auth\CategoryController@index')->name('categories.index')->middleware('auth:admin');
    Route::get('/create', 'Admin\Auth\CategoryController@create')->name('categories.create')->middleware('auth:admin');
    Route::post('/store', 'Admin\Auth\CategoryController@store')->name('categories.store')->middleware('auth:admin');
    Route::get('/show', 'Admin\Auth\CategoryController@show')->name('categories.show')->middleware('auth:admin');
    Route::get('/edit', 'Admin\Auth\CategoryController@edit')->name('categories.edit')->middleware('auth:admin');
    Route::put('/update', 'Admin\Auth\CategoryController@update')->name('categories.update')->middleware('auth:admin');
    Route::delete('/destroy', 'Admin\Auth\CategoryController@destroy')->name('categories.destroy')->middleware('auth:admin');
});

Route::prefix('products')->group(function () {
    Route::get('/home', 'User\Auth\ProductController@index')->name('products.index')->middleware('auth:user');
    Route::get('/create', 'User\Auth\ProductController@create')->name('products.create')->middleware('auth:user');
    Route::post('/store', 'User\Auth\ProductController@store')->name('products.store')->middleware('auth:user');
    Route::get('/show', 'User\Auth\ProductController@show')->name('products.show')->middleware('auth:user');
    Route::get('/edit', 'User\Auth\ProductController@edit')->name('products.edit')->middleware('auth:user');
    Route::put('/update', 'User\Auth\ProductController@update')->name('products.update')->middleware('auth:user');
    Route::delete('/destroy', 'User\AuthProductController@destroy')->name('products.destroy')->middleware('auth:user');
});*/

/*
Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        // Matches The "/admin/users" URL
    });
});*/



//Route::get('/register/admin', 'AdminARegisterController@')->name('home');

//Route::get('/home', 'HomeController@index')->name('home');
