<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::prefix('admin')->group(function () {
    Route::middleware('auth:apiAdmin')->post('register', Api\Admin\Auth\AuthController@register)->name('admin.register');
    Route::middleware('auth:apiAdmin')->post('login', Api\Admin\Auth\AuthController@login)->name('admin.login');
});*/

//Route::apiResource('/admin/categories', 'Api\Admin\Category\CategoryController');

// Category routes

Route::get('admin/categories', 'Api\Admin\Category\CategoryController@index');
Route::post('admin/categories/create', 'Api\Admin\Category\CategoryController@create');
Route::get('admin/category/{id}/show', 'Api\Admin\Category\CategoryController@show');
Route::post('/admin/category/{id}/update', 'Api\Admin\Category\CategoryController@update');
Route::post('/admin/category/{id}/delete', 'Api\Admin\Category\CategoryController@delete');


// Product routes

Route::get('admin/products', 'Api\User\Product\ProductController@index');
Route::post('admin/products/create', 'Api\User\Product\ProductController@create');
Route::get('admin/product/{id}/show', 'Api\User\Product\ProductController@show');
Route::post('/admin/product/{id}/update', 'Api\User\Product\ProductController@update');
Route::post('/admin/product/{id}/delete', 'Api\User\Product\ProductController@delete');

    // Admin Auth routes
Route::post('admin/register', 'Api\Admin\Auth\AuthController@register');
Route::post('admin/login', 'Api\Admin\Auth\AuthController@login');

    // User Auth routes
Route::post('user/register', 'Api\User\Auth\AuthController@register');
Route::post('user/login', 'Api\User\Auth\AuthController@login');

Route::get('test-get', function (Request $request) {
    return response()->json(['server-ip' => $request->ip()]);
});

Route::post('test-post', function (Request $request) {
    return response()->json([$request->username , $request->password]);
//    return response()->json([$request]);

});

/*
Route::middleware('auth:admin')->get('/user', function (Request $request) {
    return $request->user();
});*/
