<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'category_id',  'description', 'price', 'expire_at'
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
}
