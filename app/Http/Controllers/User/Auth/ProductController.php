<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $category =  Category::find(1)->category->name;
//        $products = Product::all('*');
        $products = Product::with('category')->get();
//        $products = Product::all();
        return view('user.auth.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //$categories = DB::table('categories')->get();
        $categories = Category::all();
        return view('user.auth.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => ['required', 'string', 'unique:products'],
            'description' => ['required', 'string'],
            'category_id' => ['integer'] ,
            'price' =>  ['required', 'integer'],
            'expire_at' => ['date', 'after:today']
        ]);

        Product::create($request->all());
        return redirect()->route('user.auth.products.index')
            ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        //$categories = DB::table('categories')->get();
        //return view('products.show',compact('product'));
        return view('user.auth.products.show',compact('product', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        return view('user.auth.products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $request->validate([
           'name' => ['required', Rule::unique('products')->ignore($product->id)],
            'description' => ['required', 'string'],
            'category_id' => ['integer'] ,
            'price' =>  ['required', 'integer'],
            'expire_at' => ['date']
        ]);
        $product->update($request->all());
        return redirect()->route('user.auth.products.index')
            ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();
        return redirect()->route('user.auth.products.index')
            ->with('success','Product deleted successfully');

    }
}
