<?php

namespace App\Http\Controllers\Api\Admin\Auth;


use App\Models\Admin;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    protected $redirectTo = '/home';


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $admin = Admin::create($input);
        $success['name'] = $admin->name;
        return response()->json([
            "message" => "Admin register successfully"
        ], 200);

    }

        public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()){
            return response()->json(['error' =>$validator->errors()], 401);
        }
        $verify = Admin::where('username', $request->username)->get()->first();
        if ($verify != null){
            if (Hash::check(request('password'), $verify->password)){
                //$apiAdmin = $verify;
                //$success['token'] = $apiAdmin->createToken('MyApp')->accessToken;
                return response()->json(['success' =>'You are login'], 200);
            }
            else{
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        }
        else{
            return response()->json(['error' => 'Not Found'], 404);
        }
    }
}
