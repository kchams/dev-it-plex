@extends('admin.auth.categories.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> List of categories </h2>
                <br/>
            </div>
            <div class="pull-right">
                <br/>
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Category</a>
                <hr/>
                <br/>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>
            <th width="280px">Action</th>
        </tr>

        @foreach ($categories as $category)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->description }}</td>
                <td>
                    {{--<a  href="{{ route('categories.show',$category->id) }}" class="btn btn-info">Show</a>
                    <a  href="{{ route('categories.edit',$category->id) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>--}}

                    <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('categories.show',$category->id) }}">Show</a>
                        <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    <br/> <br/>
    <div class="pull-center">
        <a class="btn btn-primary" href="{{ route('admin.home') }}"> Back</a>
    </div>
{{--    {!!categories->links() !!}--}}
@endsection
