@extends('admin.auth.categories.layout')
@section('content')
    <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
        @csrf
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
@endsection
