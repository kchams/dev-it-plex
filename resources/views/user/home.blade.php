@extends('user.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in user !
                </div>
                <div class="col-xs-3 col-sm-12 col-md-3 text-center">
                    <a class="btn btn-info" href="{{ route('products.index') }}"> Products</a>
                </div>
            </div>
        </div>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('home') }}"> Home</a>
    </div>
</div>
@endsection
