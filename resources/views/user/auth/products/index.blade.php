@extends('user.auth.products.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> List of products </h2>
                <br/>
            </div>
            <div class="pull-right">
                <br/>
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
                <hr/>
                <br/>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            {{--            <th>No</th>--}}
            <th>Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Price</th>
            <th>Expire at</th>
            <th width="280px">Action</th>
        </tr>

        @foreach ($products as $product)
            <tr>
                {{--                <td>{{ ++$i }}</td>--}}
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->category->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->expire_at }}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
                </td>
            </tr>
        @endforeach
    </table>
    <br/> <br/>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('admin.home') }}"> Back</a>
    </div>
    {{--    {!!categories->links() !!}--}}
@endsection
