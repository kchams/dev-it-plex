@extends('user.auth.products.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Sorry!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-xs-3 col-sm-12 col-md-3">
                <div class="form-group">
                    <label>Name:</label>
                    <input class="form-control" name="name" placeholder="Name" type="text"/>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label>Category :
                        <select name="category_id" id="category_id">
                        @foreach($categories as $category )
                                <option value= "{{$category->id}}" >
                                    {{$category->name}}
                                </option>
                        @endforeach
                        </select>
                    </label>
                    {{--                    <input class="form-control"  name="category_id" placeholder="Category id" type="number"/>--}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label>Description :</label>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Description"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label>Price :</label>
                    <input class="form-control"  name="price" placeholder="Price" type="number"/>
                </div>
            </div>
            <div class="col-xs-3 col-sm-12 col-md-3">
                <div class="form-group">
                    <label>Expire at : </label>
                    <input class="form-control"  name="expire_at" placeholder="expire_at" type="date"/>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </div>
    </form>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('home') }}"> Home</a>
    </div>
@endsection
